<?php
use Milleniumprod\LaravelBreadcrumb\Breadcrumb;

class Tests extends PHPUnit_Framework_TestCase
{
    protected static $title;
    protected static $link;
    protected $Breadcrumb;

    public function setUp()
    {
        self::$title = self::text(20);
        self::$link = 'http://'.self::text(20);

        $this->Breadcrumb = new Breadcrumb([
            'separator' => '>',
        ]);
    }

    protected static function text($length)
    {
        $base = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $text = '';

        while (mb_strlen($text) < $length) {
            $text .= str_shuffle($base);
        }

        return mb_substr($text, 0, $length);
    }


    public function testAddCrumb()
    {
        $this->Breadcrumb->add(self::$title,self::$link));
    }

}
