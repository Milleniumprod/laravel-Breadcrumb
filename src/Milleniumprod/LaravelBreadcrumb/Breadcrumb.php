<?php
namespace Milleniumprod\LaravelBreadcrumb;

use App;
use Lang;

class Breadcrumb
{

    /**
     * @var array
     */
    private $config = [];

    /**
     * @var array
     */
    private $crumbs = [];

    /**
     * @var array
     */
    private $defaults = [
        'separator' => '',
        'container_tag' => 'ol',
        'container_class' => 'breadcrumb',
        'child_tag' => 'li',
        'child_class' => '',
    ];

    /**
     * @var array
     */
    private $processed = [];

    /**
     * @var object;
     */
    private static $instance;

    /**
     * @param  array $config
     * @return object
     */
    public static function getInstance(array $config = [])
    {
        return static::$instance ?: (static::$instance = new self($config));
    }

    /**
     * @param  array $config
     *
     * @return this
     */
    public function __construct($config = [])
    {
        if ($config) {
            $this->setConfig($config);
        }


        return $this;
    }

    /**
     * @param  array $config
     *
     * @return this
     */
    public function setConfig(array $config = [])
    {
        $config = $config + $this->config;

        foreach ($this->defaults as $key => $value) {
            if (!array_key_exists($key, $config)) {
                $config[$key] = $value;
            }
        }

        $this->config = $config;

        return $this;
    }

    /**
     * @param  string $value
     *
     * @return string
     */
    public function home($value)
    {

        $link =   App::getLocale() == Lang::getFallback()?'/':'/'.App::getLocale();
        $this->crumbs[$link] = $value;

        return $this;
    }

    /**
     * @param  string $value
     * @param  string link
     *
     * @return string
     */
    public function add($value,$link)
    {

        $this->crumbs[$link] = $value;

        return $this;
    }

    /**
     * @param  string       $key
     * @param  string|array $default
     *
     * @return string
     */
    public function get($key, $default = null)
    {
        $method = 'get'.$key;

        if (method_exists($this, $method)) {
            return $this->$method($default);
        }

        if (empty($this->crumbs[$key])) {
            return $default;
        }

        return $this->crumbs[$key];
    }

    /**
     *
     * @return string
     */
    public function render()
    {
        $html = (empty($this->config["container_class"]))?"<".$this->config['container_tag'].">":"<".$this->config['container_tag']." class='".$this->config["container_class"]."'>";
        $i=0;
        foreach ( $this->crumbs as $link => $value) {

            $i++;
            $html .= (empty($this->config["child_class"]))?"<".$this->config['child_tag'].">":"<".$this->config['child_tag']." class='".$this->config["child_class"]."'>";
            $html .= ( $i == count($this->crumbs) )?$value:"<a href='".$link."'>".$value."</a>";
            $html .= ( $i == count($this->crumbs) )?'':$this->config['separator'];
            $html .= "</".$this->config['child_tag'].">";
        }

        $html .= "</".$this->config['container_tag'].">";

        return $html;
    }
}
